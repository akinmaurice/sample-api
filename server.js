const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.send('Welcome to the API service!');
});

app.listen(3069, () => console.log('Service running on port 3069'));
